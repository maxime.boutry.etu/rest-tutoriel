package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.DetailTauxDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path; // !!!!!!
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut") // http://localhost:8080/api/v1/taux/tauxpardefaut
    public double getValeurTauxParDefaut() {
            return TauxTva.NORMAL.taux;
    }
    
    //ATTENTION Relié à l'enum TauxTva : pas vraiment le choix : NORMAL, INTERMEDIAIRE REDUIT PARTICULIER
/*    @GET
    @Path("valeur/{niveauTva}") // http://localhost:8080/api/v1/taux/valeur
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
      return TauxTva.valueOf(niveau.toUpperCase()).taux;
    }
*/
    
    //getValeurTaux v2
    @GET
    @Path("valeur/{niveauTva}") // http://localhost:8080/api/v1/taux/valeur
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }

    
    @GET
    @Path("{niveauTva}")
    public double getMontantTotal(@PathParam("niveauTva") String niveau, @QueryParam("somme") int somme ) {
    	try {
    		return getValeurTaux(niveau)+somme;
    	} catch(Exception ex) {
    		throw new NiveauTvaInexistantException();
    	}
    
    }
    
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }

    @GET
    @Path("details/{niveauTva}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DetailTauxDto getDetail(@PathParam("niveauTva") String niveau, @QueryParam("somme") int somme ) {
    	try {
    		return new DetailTauxDto(getMontantTotal(niveau, somme), TauxTva.fromString(niveau.toUpperCase()).taux, somme, niveau, getValeurTaux(niveau));
    	} catch(Exception ex) {
    		throw new NiveauTvaInexistantException();
    	}
    
    }
    
}




